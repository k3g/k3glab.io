---
layout: k3g_layout.njk
---
<k3g-small-logo></k3g-small-logo>

# Pré-requis


> Selon Louis (pro Linux) il faut Linux ou OSX avec **Docker** et **Git** (ce peut être une VM) avec au moins 4 CPU et 6 GO RAM (gitlab c’est gourmand 😅).

> Selon Philippe (pro Mac), essayez quelque chose d’un peu plus costaud au niveau de la RAM (8 c’est bien ... enfin ..., 16 c’est mieux)

> Tout ça pour dire, que si vous êtes sous Linux ça devrait mieux se passer 😉

## Remarques: 

> - Si vous venez avec du Windows si ça ne marche pas, on ne pourra rien pour vous. 
> - Si vous venez avec un Linux 32, on ne pourra rien pour vous non plus
> - Si vous n’avez pas installé les prérequis on ne pourra pas vous attendre (pensez aux autres)
> - Si vous n’avez pas d’ordinateur, cela reste intéressant et on vous donnera tous les éléments pour le reproduire chez vous

<return-to-home></return-to-home>