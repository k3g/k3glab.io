---
layout: k3g_layout.njk
---

<k3g-logo></k3g-logo>

## Qu'est-ce que K3G? 

> **La version courte**: K3G, c'est une instance **GitLab** 🦊 qui "tourne" sur **K3S** (un Kubernetes allégé) et le tout "emballé" dans **Docker** 🐳... tournant en local 😍

> **La version longue**: **K3G**, c'est nous, (Louis et Philippe), après un très long brain storming qui avont décidés d'appeler notre projet **K3G**. Ce projet, c'est plusieurs choses à la fois:
> - un **workshop** (hands-on pour être précis) qui va vous permettre de créer un cluster kube, d'y déployer un GitLab et un runner, et enfin d'utiliser GitLab CI pour déployer des webapplications sur ce même cluster. 🖐 **Et tout ça en local sur votre machine!**
> - un ensemble de **scripts** qui vous permettront de rejouer le tout à la maison (facilement)
> - quelques **exemples** d'applications à déployer avec les scripts de **CI** associés

> Pour simplifier, (gagner un peu de ressources machines aussi, parce il faut garder "un peu" de place pour **[GitLab](https://about.gitlab.com/)**), nous utilisons la distribution Kubernetes allégée **[K3S](https://k3s.io/) (de chez Rancher, qui peut tourner sur un RPI)**, que nous déployons dans du Docker à l'aide du projet **[K3D](https://github.com/rancher/k3d) (de chez Rancher aussi)**

## Objectif(s)

> - A la fin du workshop vous devez être capable de remonter l'ensemble de la plateforme tout seul
> - Pouvoir disposer, à des fins d'autoformation, d'une plateforme de développement et d'exécution directement sur votre machine (il y a quelques contraintes, n'oubliez pas d'aller lire les [prérequis](prerequisites))
> - Avoir une plateforme évolutive pour installer d'autres applications (OpenFaaS, Rio, Bases de données, ...)

> **Donc si vous aimez expérimentez, si vous voulez déployer des micro-services "comme en vrai" mais sur votre machine, ... Venez apprendre à "monter votre infra"**

## Les origines

> **Louis** se débrouille plutôt bien en Kubernetes (en même temps il a été bien formé 😉) et souvent nos brains storming au restaurant se concluent par une "idée à la 💥". Cette fois ci, ça a été *"mais est-ce que GitLab ça tournerait sur K3S?"* 🤔.

> **Philippe** qui est au niveau "lecture de magazines" en ce qui concerne Kubernetes (+3 livres non commencés dans un placard et quelques cours sur internet ... pas commencés non plus) a tout de suite sauté sur l'occasion: *"tu saurais faire?"* (sur la partie GitLab, **Philippe** se débrouille plutôt bien, mais n'arrive pas à faire un setup correct avec Kubernetes).

> Bref, à la fin du resto, ça s'est transformé en **"on va faire un workshop pour que les devs (mais pas que) puissent monter un cluster Kube, mais continuer ensuite et réellement s'en servir"** (on va aller un chouilla plus loin que le **👋Hello World🌍** *... pas non plus trop loin, ou alors on transforme ça en formation... mince nouvelle idée 😜*)

## Qu'est ce que vous allez découvrir en détail?

> Pas d'inquiétude, tout est documenté, il vous suffira de suivre les étapes pas à pas, guidés avec bienveillance par **Louis** (**Philippe** est un ours 🐻 tendance 🐼, donc pour la bienveillance, il faut le nourrir avant 🥙🍺)

> Si vous avez un doute quant à la difficulté:
> - **Philippe** a réussi à terminer le workshop (mais on n'a pas chronométré)
> - Si vous n'arrivez pas à terminer, ce n'est pas grave, tous les éléments pour continuer seul seront fournis (et aussi un lien vers le projet pour pouvoir ouvrir des **"issues"** si vous avez des questions)

### "Plan"

> 👋🚧 Le plan peut légèrement changer, notamment si nous sommes chauds lors de nos prochains restos, nous aurons de nouvelles idées

> - Introduction
> - Installer les outils
> - Créer un cluster vide
> - Installer un local storage
> - Installer Traefik
> - Installer les Métriques
> - Installer GitLab
> - Configurer GitLab
> - Utiliser GitLab
> - Utiliser GitLab: déployer une web application
> - Utiliser GitLab: mise en oeuvre d'une review application
> - 🎉

Maintenant, allez prendre connaissance [des prérequis](prerequisites).


<nav-bar></nav-bar>
