---
layout: k3g_layout.njk
---
<k3g-small-logo></k3g-small-logo>

# Qui sommes nous?

## Louis Tournayre

> **Job**:
> - DT et aussi Formateur, Dev Backend, DevOps @ Zenika Lyon

> **Quoi d'autre?**
> - 100% Papa 🐔
> - Activités: Papa 🐔, Geek, Cuisine, Bricolage, Livres & BDD
> - Signe particulier: Fait le deuil du Front (même au Baby Foot je suis nul à l'avant)
> - [GDG Cloud et IOT Lyon](https://www.meetup.com/fr-FR/GDG-Cloud-Lyon/) co leader
> - Ex ScalaIO team member

>🐦[@_louidji](http://twitter.com/_louidji)

## Philippe Charrière

> **Jobs**:
> - Senior Technical Account Manager at GitLab 🦊
> - CEO en sommeil at Bots.Garden 🤖

> **Quoi d'autre?**
> - Plat préféré: Pot au feu 🍲
> - Cocktail préféré: Spritz 🍸
> - Activités: Geek, Pêche, Cuisine, Série TV 🎣
> - Signe particulier: grosse addiction aux emojis
> - [GDG Cloud et IOT Lyon](https://www.meetup.com/fr-FR/GDG-Cloud-Lyon/) co leader
> - BlendWebMix Responsable du comité éditorial technique
> - Ex ScalaIO team member, Ex MixIT team member

>🐦[@k33g_org](http://twitter.com/k33g_org)

<return-to-home></return-to-home>
