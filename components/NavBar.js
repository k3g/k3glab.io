class NavBar extends HTMLElement {
  
  connectedCallback() {
    this.attachShadow({mode: 'open'})
    this.shadowRoot.adoptedStyleSheets = [window.chota]
    this.render()
  }

  render() {
		return this.html(`
      <nav class="tabs is-center">
        <a href="who">Nous?</a>
        <a href="prerequisites">Prérequis</a>
        <a href="documentation">Documentation</a>
        <!--
        <a href="resources">Ressources</a>
        -->
      </nav>
		`)
  }

  html(content) {
		this.shadowRoot.innerHTML = content
  }

}

customElements.define('nav-bar', NavBar)




