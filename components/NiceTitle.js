console.log("👋 I'm here")
class NiceTitle extends HTMLElement {
  
  connectedCallback() {
    this.attachShadow({mode: 'open'})
    this.shadowRoot.adoptedStyleSheets = [window.chota]
    this.render()
  }

  render() {
		return this.html(`
      <div class="hero is-center is-vertical-align">
        <h1>${this.getAttribute("text")}</h1>
      </div>
		`)
  }

  html(content) {
		this.shadowRoot.innerHTML = content
  }

}

customElements.define('nice-title', NiceTitle)




