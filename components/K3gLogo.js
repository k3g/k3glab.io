class K3gLogo extends HTMLElement {
  
  connectedCallback() {
    this.attachShadow({mode: 'open'})
    this.shadowRoot.adoptedStyleSheets = [window.chota]
    this.render()
  }

  render() {
		return this.html(`
      <div class="hero">
        <nav-bar></nav-bar>
        <div class="logo is-center is-vertical-align">
          <img src="./pictures/k3g_001.png" alt="" style="width:70%; height:70%;">
        </div>
      </div>
		`)
  }

  html(content) {
		this.shadowRoot.innerHTML = content
  }

}

customElements.define('k3g-logo', K3gLogo)




