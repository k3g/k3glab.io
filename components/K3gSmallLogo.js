class K3gSmallLogo extends HTMLElement {
  
  connectedCallback() {
    this.attachShadow({mode: 'open'})
    this.shadowRoot.adoptedStyleSheets = [window.chota]
    this.render()
  }

  render() {
    return this.html(`
      <div class="hero">
        <return-to-home></return-to-home>
        <div class="logo is-center is-vertical-align">
          <img src="/pictures/k3g_001.png" alt="" style="width:20%; height:20%;">
        </div>
      </div>    
		`)
  }

  html(content) {
		this.shadowRoot.innerHTML = content
  }

}

customElements.define('k3g-small-logo', K3gSmallLogo)




