class ReturnToHome extends HTMLElement {
  
  connectedCallback() {
    this.attachShadow({mode: 'open'})
    this.shadowRoot.adoptedStyleSheets = [window.chota]
    this.render()
  }

  render() {
    return this.html(`
    <nav class="tabs is-center">
      <a href="/">Retour</a>
      <a href="/prerequisites">Prérequis</a>
      <a href="/documentation">Documentation</a>
    </nav>  
		`)
  }

  html(content) {
		this.shadowRoot.innerHTML = content
  }

}

customElements.define('return-to-home', ReturnToHome)




