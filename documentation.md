---
layout: k3g_layout.njk
---
<k3g-small-logo></k3g-small-logo>

# Documentation

> - Codelab 🇫🇷 en place (utilisation de [googlecodelabs](https://github.com/googlecodelabs/tools)): ✅
> - Scripts: ✅
> - Documentation 🇫🇷 (version "écrite" du codelab):  🚧 WIP

> - Codelab 🇬🇧: TODO
> - Documentation 🇬🇧: TODO

<return-to-home></return-to-home>