module.exports = function(eleventyConfig) {
  eleventyConfig.addPassthroughCopy('css')
  eleventyConfig.addPassthroughCopy('js')
  eleventyConfig.addPassthroughCopy('components')
  eleventyConfig.addPassthroughCopy('pictures')

  return {
    passthroughFileCopy: true
  }
}